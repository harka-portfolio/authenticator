function dec2hex(s) {
  return (s < 15.5 ? '0' : '') + Math.round(s).toString(16)
}

function hex2dec(s) {
  return parseInt(s, 16)
}

function base32tohex(base32) {
  const base32chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'
  let bits = ''
  let hex = ''

  for (let i = 0; i < base32.length; i++) {
    const val = base32chars.indexOf(base32.charAt(i).toUpperCase())
    bits += leftpad(val.toString(2), 5, '0')
  }

  for (let i = 0; i + 4 <= bits.length; i += 4) {
    const chunk = bits.substr(i, 4)
    hex = hex + parseInt(chunk, 2).toString(16)
  }
  return hex
}

function leftpad(str, len, pad) {
  if (len + 1 >= str.length) {
    str = Array(len + 1 - str.length).join(pad) + str
  }
  return str
}

function updateOtp() {
  const secretValue = document.getElementById('secret').value
  const key = base32tohex(secretValue)
  const epoch = Math.round(new Date().getTime() / 1000.0)
  const time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, '0')

  // updated for jsSHA v2.0.0 - http://caligatio.github.io/jsSHA/
  const shaObj = new jsSHA('SHA-1', 'HEX')
  shaObj.setHMACKey(key, 'HEX')
  shaObj.update(time)
  const hmac = shaObj.getHMAC('HEX')

  document.getElementById('qrImg').src =
    'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=200x200&chld=M|0&cht=qr&chl=otpauth://totp/user@host.com%3Fsecret%3D' +
    secretValue
  document.getElementById('secretHex').textContent = key
  document.getElementById('secretHexLength').textContent =
    key.length * 4 + ' bits'
  document.getElementById('epoch').textContent = time
  document.getElementById('hmac').innerHTML = ''

  if (hmac == 'KEY MUST BE IN BYTE INCREMENTS') {
    const spanElement = document.createElement('span')
    spanElement.className = 'label important'
    spanElement.appendChild(document.createTextNode(hmac))
    document.getElementById('hmac').appendChild(spanElement)
  } else {
    const offset = hex2dec(hmac.substring(hmac.length - 1))
    const part1 = hmac.substr(0, offset * 2)
    const part2 = hmac.substr(offset * 2, 8)
    const part3 = hmac.substr(offset * 2 + 8, hmac.length - offset)

    if (part1.length > 0) {
      const spanElement1 = document.createElement('span')
      spanElement1.className = 'label label-default'
      spanElement1.appendChild(document.createTextNode(part1))
      document.getElementById('hmac').appendChild(spanElement1)
    }

    const spanElement2 = document.createElement('span')
    spanElement2.className = 'label label-primary'
    spanElement2.appendChild(document.createTextNode(part2))
    document.getElementById('hmac').appendChild(spanElement2)

    if (part3.length > 0) {
      const spanElement3 = document.createElement('span')
      spanElement3.className = 'label label-default'
      spanElement3.appendChild(document.createTextNode(part3))
      document.getElementById('hmac').appendChild(spanElement3)
    }
  }

  let otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) + ''
  otp = otp.substr(otp.length - 6, 6)

  document.getElementById('otp').textContent = otp
}

function timer() {
  const epoch = Math.round(new Date().getTime() / 1000.0)
  const countDown = 30 - (epoch % 30)
  if (epoch % 30 == 0) updateOtp()
  document.getElementById('updatingIn').textContent = countDown
}

document.addEventListener('DOMContentLoaded', function () {
  updateOtp()

  document.getElementById('update').addEventListener('click', function (event) {
    updateOtp()
    event.preventDefault()
  })

  document.getElementById('secret').addEventListener('keyup', function () {
    updateOtp()
  })

  setInterval(timer, 1000)
})
