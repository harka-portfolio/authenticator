let data = localStorage.data ? JSON.parse(localStorage.data) : []
const infoView = document.querySelector('article')
const mainView = document.querySelector('main')
const timerElement = mainView.querySelector('.timer')
const filterInput = mainView.querySelector('search input')
const editCheckbox = mainView.querySelector('input[type="checkbox"]')
const service = mainView.querySelector('input[placeholder="Service"]')
const secret = mainView.querySelector('input[placeholder="Secret"]')
const addButton = mainView.querySelector('button')
const settingsView = document.querySelector('section')
const filterCheck = settingsView.querySelector('input[type="checkbox"]')
const exportButton = settingsView.querySelectorAll('button')[0]
const importButton = settingsView.querySelectorAll('button')[1]
const homeButton = document.querySelectorAll('footer button')[0]
const settingsButton = document.querySelectorAll('footer button')[1]
const infoButton = document.querySelectorAll('footer button')[2]

let filterOption = localStorage.hideFilter
const dec2hex = s => (s < 15.5 ? '0' : '') + Math.round(s).toString(16)
const hex2dec = s => parseInt(s, 16)
const edit = () => editCheckbox.click()

const setFilterOption = () => {
  if (filterOption) filterCheck.checked = true
  else filterCheck.checked = false
  toggleFilter()
}

const toggleFilter = () => {
  if (filterCheck.checked) {
    filterInput.setAttribute('hidden', '')
    filterOption = 1
  } else {
    filterInput.removeAttribute('hidden')
    filterOption = ''
  }
  localStorage.hideFilter = filterOption
}

const showMain = () => {
  infoView.setAttribute('hidden', '')
  settingsView.setAttribute('hidden', '')
  mainView.removeAttribute('hidden')
  filterInput.focus()
}

const showSettings = () => {
  mainView.setAttribute('hidden', '')
  infoView.setAttribute('hidden', '')
  settingsView.removeAttribute('hidden')
}

const showInfo = () => {
  mainView.setAttribute('hidden', '')
  settingsView.setAttribute('hidden', '')
  infoView.removeAttribute('hidden')
}

const base32tohex = base32 => {
  const base32chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'
  let bits = ''
  let hex = ''

  for (let i = 0; i < base32.length; i++) {
    const val = base32chars.indexOf(base32.charAt(i).toUpperCase())
    bits += leftPad(val.toString(2), 5, '0')
  }

  for (i = 0; i + 4 <= bits.length; i += 4) {
    const chunk = bits.substring(i, i + 4)
    hex = hex + parseInt(chunk, 2).toString(16)
  }

  return hex
}

const leftPad = (string, length, pad) => {
  return new Array(length + 1 - string.length).join(pad) + string
}

const generate = secret => {
  let key = base32tohex(secret)

  // HMAC generator requires secret key to have even number of nibbles
  if (key.length % 2 !== 0) key += '0'

  const time = leftPad(dec2hex(Math.floor(epoch / 30)), 16, '0')

  // External library for SHA functionality
  const hmacObj = new jsSHA(time, 'HEX')
  const hmac = hmacObj.getHMAC(key, 'HEX', 'SHA-1', 'HEX')

  let offset = 0
  if (hmac !== 'KEY MUST BE IN BYTE INCREMENTS') {
    offset = hex2dec(hmac.substring(hmac.length - 1))
  }

  const otp =
    ((hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) % 1000000) + ''

  return Array(7 - otp.length).join('0') + otp
}

const copy = (target, id) => {
  navigator.clipboard
    .writeText(target.textContent)
    .then(() => {
      target.textContent = 'copied'
      setTimeout(() => {
        target.textContent = generate(data[id][1])
      }, 1000)
    })
    .catch(err => {
      alert('Error: ', err)
      target.textContent = generate(data[id][1])
    })
}

const sort = () => {
  data.sort((a, b) => {
    let elementA = a[0].toLowerCase()
    let elementB = b[0].toLowerCase()
    if (elementA < elementB) return -1
    if (elementA > elementB) return 1
    return 0
  })
}

const add = () => {
  data.push([service.value, secret.value])
  service.value = ''
  secret.value = ''
  sort()
  localStorage.data = JSON.stringify(data)
  render()
}

const remove = id => {
  if (confirm(`${data[id][0] || 'This element'} will be deleted.`) === true) {
    data.splice(id, 1)
    localStorage.data = JSON.stringify(data)
    render()
  }
}

const exportData = () => {
  navigator.clipboard
    .writeText(localStorage.data)
    .then(() => {
      exportButton.textContent = 'Copied to clipboard'
      setTimeout(() => {
        exportButton.textContent = 'Export data'
      }, 2000)
    })
    .catch(err => {
      alert('Unable to copy text to clipboard', err)
    })
}

const importData = () => {
  const importField = settingsView.querySelector('textarea')
  try {
    data = data.concat(JSON.parse(importField.value))
    localStorage.data = JSON.stringify(data)
    importButton.textContent = 'Succesful import'
    importField.value = ''
    sort()
    render()
  } catch (err) {
    importButton.textContent = 'Invalid data'
  } finally {
    setTimeout(() => {
      importButton.textContent = 'Import data'
    }, 2000)
  }
}

const filter = () => {
  const filterTerm = filterInput.value.toUpperCase()
  mainView.querySelectorAll('h3').forEach(name => {
    if (name.textContent.toUpperCase().includes(filterTerm)) {
      name.parentElement.style.display = ''
    } else {
      name.parentElement.style.display = 'none'
    }
  })
}

const render = () => {
  let html = ''
  for (let i = 0; i < data.length; i++) {
    const key = data[i]
    html += `<div id="${i}">
    <button class="edit">delete</button>
    <h3>${key[0]}</h3>
    <button class="otp">${generate(key[1])}</button>
    <br>___________________<br><br></div>`
  }
  mainView.querySelector('p').innerHTML = html
  if (filterInput.value) filter()
}

const timerTick = () => {
  epoch = Math.round(new Date().getTime() / 1000.0)
  const countDown = (30 - (epoch % 30)).toString().padStart(2, '0')
  if (epoch % 30 === 0) render()
  timerElement.textContent = countDown
}

homeButton.addEventListener('click', showMain)
settingsButton.addEventListener('click', showSettings)
infoButton.addEventListener('click', showInfo)
addButton.addEventListener('click', add)
filterInput.addEventListener('keyup', filter)
filterCheck.addEventListener('click', toggleFilter)
exportButton.addEventListener('click', exportData)
importButton.addEventListener('click', importData)

editCheckbox.addEventListener('click', () => {
  if (editCheckbox.checked) mainView.classList.remove('readonly')
  else mainView.classList.add('readonly')
})

mainView.addEventListener('click', e => {
  const target = e.target
  const id = target.parentElement.id
  if (target.matches('.otp')) copy(target, id)
  else if (target.matches('.edit')) remove(id)
})

const start = () => {
  if (data.length) showMain()
  else edit()
  setInterval(timerTick, 500)
  timerTick()
  render()
  setFilterOption()
}

start()
